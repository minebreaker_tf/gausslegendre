# -*- coding: utf-8 -*-


import sys

import math

u"""Gauss-Legendre
Calculate pi value using Gauss-Legendre algorithm
The calculation is not precise because of floating point number!
TODO rewrite using sympy!
TODO use cache
"""


def main(args):
    u"""Main
    arg[0] : precision. default = 3
    """
    num = args[0] if type(args[0]) == int else 3
    print(pi(num))
    print(math.pi)


def pi(n):
    return math.pow(a(n) + b(n), 2) / (4 * t(n))


def a(n):
    if n == 0:
        return 1
    else:
        return (a(n - 1) + b(n - 1)) / 2


def b(n):
    if n == 0:
        return 1 / math.sqrt(2)
    else:
        return math.sqrt(a(n - 1) * b(n - 1))


def t(n):
    if n == 0:
        return 1 / 4
    else:
        return t(n - 1) - p(n - 1) * math.pow((a(n - 1) - a(n)), 2)


def p(n):
    if n == 0:
        return 1
    else:
        return 2 * p(n - 1)


if __name__ == '__main__':
    main(sys.argv)
